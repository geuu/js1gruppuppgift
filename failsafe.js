//läser in alla grundelement
const bookSection = document.querySelector("#bookSection");
const editBtn = document.querySelector("#edit");
//funktion för att lägga till bok
const addBook = (position, obj) => {
  bookSection.insertAdjacentHTML(
    position,
    `<article class="bookArticle"><h2>${obj.title}</h2><h3>${obj.author}</h3><img src="${obj.img}" class="bookimg"><h4>${obj.genre}</h4><p>${obj.info}</p></article>`
  );
};
//hämtar och visar böcker från jsonfilen
fetch("books.json")
  .then(res => res.json())
  .then(data =>
    data.forEach(obj => {
      addBook("afterbegin", obj);
    })
  );
//funktion för att visa alla Deleteknappar
function showX() {
  const books = document.querySelectorAll(".bookArticle");
  books.forEach(book => {
    book.insertAdjacentHTML(
      "afterbegin",
      `<button class="deleteBtn">X</button>`
    );
  });
  const deleteBtns = document.querySelectorAll(".deleteBtn");
  //eventlistener som tar bort den bok vars deleteknapp man trycker på
  books.forEach((book, index) => {
    deleteBtns[index].addEventListener("click", () => {
      bookSection.removeChild(book);
    });
  });
}
//funktion för att dölja alla Deleteknappar
function hideX() {
  const books = document.querySelectorAll(".bookArticle");
  books.forEach(book => {
    book.removeChild(book.firstChild);
  });
}
//eventlistener som startar redigeraläget
editBtn.addEventListener("click", () => {
  editMode();
});
//funktion för att starta redigeraläget
function editMode() {
  bookSection.insertAdjacentHTML(
    "beforeend",
    `<article id="editArticle" class="active"><button id="addBook">+</button><button id="done">Done Editing</button></article>`
  );
  editBtn.disabled = true;
  showX();
  document.querySelector("#done").addEventListener("click", () => {
    editBtn.disabled = false;
    hideX();
    bookSection.removeChild(bookSection.lastChild);
  });
  document.getElementById("addBook").addEventListener("click", () => {
    const editArticle = document.querySelector("#editArticle");
    editArticle.classList.remove("active");
    editArticle.innerHTML = `       <form id="addForm">
    <label for="title">Title:</label>
    <input type="text" name="title" id="title" required>
    <label for="author">Author:</label>
    <input type="text" name="author" id="author" required>
    <label for="genre">Genre:</label>
    <input type="text" name="genre" id="genre" required>
    <label for="info">Info:</label>
    <input type="text" name="info" id="info" required>
    <div id="addImage">Choose image</div>
    <button type="submit">Submit</button>
    </form>
    <button id="arrow" class="arrow"><i class="fa-solid fa-circle-arrow-left"></i></button>  `;
    let imageChoice ="";
    document.querySelector("#addImage").addEventListener("click",()=>{
      const imgBox = document.createElement("div")
      imgBox.id="imgBox"
      randomimages.forEach(img=>{
        imgBox.appendChild(img)
        img.addEventListener("click",()=>{
          imageChoice=img.src
          editArticle.querySelector("#addImage").style.backgroundImage = `url(${imageChoice})`
          editArticle.querySelector("#addImage").classList.add("active")
          imgBox.remove()
      })
      editArticle.appendChild(imgBox)
  
      })
      })

    const form = document.querySelector("#addForm");
    form.addEventListener("submit", e => {
      e.preventDefault();
      const book = {
        title: e.target.title.value.replace(/<[^>]*>/g, ""),
        author: e.target.author.value.replace(/<[^>]*>/g, ""),
        genre: e.target.genre.value.replace(/<[^>]*>/g, ""),
        info: e.target.info.value.replace(/<[^>]*>/g, ""),
        img: imageChoice,
      };
      bookSection.removeChild(bookSection.lastChild);
      hideX();
      editBtn.disabled = false;
      addBook("beforeend", book);
    });

    document.getElementById("arrow").addEventListener("click", () => {
      bookSection.removeChild(bookSection.lastChild);
      hideX();
      editMode();
    });
  });
}

// https://picsum.photos/
const randomnumber = Math.floor(Math.random()*50)
const randomimages = []

fetch(`https://picsum.photos/v2/list?page=${randomnumber}&limit=12`)
  .then(res => res.json())
  .then(data =>{data.forEach(obj=>{
    const newUrl = obj.download_url.split("/")
    newUrl[newUrl.length-1] = "512"
    newUrl[newUrl.length-2] = "512"
    const imageElement = document.createElement("img")
    imageElement.src=newUrl.join("/")
    randomimages.push(imageElement)
  })})




fetch("https://www.googleapis.com/books/v1/volumes?q=harry+potter",{
  "key":"AIzaSyCkNnYSUf7C6lU8WH_gb16InDtz4tyUI0g"
})
.then(
  res => res.json()
)
.then(
  data=>{
   
    console.log(data)
    
  }
)