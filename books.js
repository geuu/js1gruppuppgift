//läser in alla grundelement
const bookSection = document.querySelector("#bookSection");
const editBtn = document.querySelector("#edit");
//funktion för att lägga till bok
const addBook = (position, obj) => {
  bookSection.insertAdjacentHTML(
    position,
    `<article class="bookArticle"><h2>${obj.title}</h2><h3>${obj.author}</h3><img src="${obj.img}" class="bookimg"><h4>${obj.genre}</h4><p>${obj.info}</p></article>`
  );
};
//hämtar och visar böcker från jsonfilen
fetch("books.json")
  .then((res) => res.json())
  .then((data) =>
    data.forEach((obj) => {
      addBook("afterbegin", obj);
    })
  );
//funktion för att visa alla Deleteknappar
function showX() {
  const books = document.querySelectorAll(".bookArticle");
  books.forEach((book) => {
    book.insertAdjacentHTML(
      "afterbegin",
      `<button class="deleteBtn">X</button>`
    );
  });
  const deleteBtns = document.querySelectorAll(".deleteBtn");
  //eventlistener som tar bort den bok vars deleteknapp man trycker på
  books.forEach((book, index) => {
    deleteBtns[index].addEventListener("click", () => {
      bookSection.removeChild(book);
    });
  });
}
//funktion för att dölja alla Deleteknappar
function hideX() {
  const books = document.querySelectorAll(".bookArticle");
  books.forEach((book) => {
    book.removeChild(book.firstChild);
  });
}
//eventlistener som startar redigeraläget
editBtn.addEventListener("click", () => {
  editMode();
});
//funktion för att starta redigeraläget
function editMode() {
  bookSection.insertAdjacentHTML(
    "beforeend",
    `<article id="editArticle" class="active"><button id="addBook">+</button><button id="done">Done Editing</button></article>`
  );
  editBtn.disabled = true;
  showX();
  document.querySelector("#done").addEventListener("click", () => {
    editBtn.disabled = false;
    hideX();
    bookSection.removeChild(bookSection.lastChild);
  });

  //när vi klickar på add book så visas ett formulär
  document.getElementById("addBook").addEventListener("click", () => {
    const editArticle = document.querySelector("#editArticle");
    editArticle.classList.remove("active");
    editArticle.innerHTML = `       <form id="addForm">
    <label for="title">Title:</label>
    <input type="text" name="title" id="title" required>
    <div id ="bookSuggestions"></div>
    <label for="author">Author:</label>
    <input type="text" name="author" id="author" required>
    <label for="genre">Genre:</label>
    <input type="text" name="genre" id="genre" required>
    <label for="info">Info:</label>
    <input type="text" name="info" id="info" required>
    <div id="addImage">Choose image</div>
    <button type="submit">Submit</button>
    </form>
    <button id="arrow" class="arrow"><i class="fa-solid fa-circle-arrow-left"></i></button>  `;
    let imageChoice = "";
    //när vi ändrar i titeln fetchas böcker från googles api och
    //visas som förslag på skärmen
    editArticle.querySelector("#title").addEventListener("input", () => {
      const search = editArticle.querySelector("#title").value;
      if (search.length > 0) {
        fetch(`https://www.googleapis.com/books/v1/volumes?q=${search}`, {
          key: "AIzaSyCkNnYSUf7C6lU8WH_gb16InDtz4tyUI0g",
        })
          .then((res) => {
            if(!res.ok){
              throw new Error("FEL I FETCH REQUEST");
            }
            return res.json();
          })
          .catch()
          .then((data) => {
            const books = document.querySelectorAll(".bookFetch");

            //om datan vi fetchar in innehåller böcker
            let containsItems = false;
            Object.entries(data).forEach((array)=>{
              array.forEach(subArray=>{
                if(subArray ==="items"){
                  containsItems=true;
                  
                }
              })
            })
            //funktion för att kolla så indatan från apin är ok
            const getBookObj = (i) => {
              if (containsItems) {
                let containsVolumeInfo = false;
                if (typeof data.items[i]==="object"){
                  Object.entries(data.items[i]).forEach(array=>{
                    array.forEach(subArray=>{
                      if(subArray==="volumeInfo"){
                        containsVolumeInfo=true;
                      }
                    })
                  })
                }
                //om böckerna i datan vi fetchar in innehåller volume info
                if (containsVolumeInfo) {
                  const title = data.items[i].volumeInfo.title
                    ? data.items[i].volumeInfo.title
                    : "Title Not Found";
                  const author = data.items[i].volumeInfo.authors
                    ? data.items[i].volumeInfo.authors
                    : "Authors Not Found";
                  let image = "images/logo.png";
                  let smallImage = "images/logo.png";
                  if (
                    JSON.stringify(data.items[i].volumeInfo).includes(
                      '"imageLinks"'
                    )
                  ) {
                    image = data.items[i].volumeInfo.imageLinks.thumbnail
                      ? data.items[i].volumeInfo.imageLinks.thumbnail
                      : "images/logo.png";
                    smallImage = data.items[i].volumeInfo.imageLinks
                      .smallThumbnail
                      ? data.items[i].volumeInfo.imageLinks.smallThumbnail
                      : "images/logo.png";
                  }
                  const genre = data.items[i].volumeInfo.categories
                    ? data.items[i].volumeInfo.categories
                    : "Genre Not Found";
                  const info = data.items[i].volumeInfo.description
                    ? data.items[i].volumeInfo.description
                    : "Description Not Found";
                  
                  return {
                    title: title,
                    author: author,
                    image: image,
                    smallImage: smallImage,
                    genre: genre,
                    info: info,
                  };
                }
              }
              //default objektet om där är något fel
              return {
                title: "No Results",
                author: "No Results",
                info: "No Results",
                image: "images/logo.png",
                smallImage: "images/logo.png",
                genre: "No Results",
              };
            };

            if (!books.length) {
              for (let i = 0; i < 10; i++) {
                editArticle
                  .querySelector("#bookSuggestions")
                  .insertAdjacentHTML(
                    "afterbegin",
                    `<div class="bookFetch"></div>`
                  );
              }
            } else {
              editArticle.querySelector("#bookSuggestions").style.display = "block"
              books.forEach((book, i) => {
                book.innerHTML = `<img src="${getBookObj(i).smallImage}" alt="" class="fetchImg"><div class=bookFetchInfo><h2 class="titleClass">${getBookObj(i).title}</h2><h3>${getBookObj(i).author}</h3></div>`;
                book.addEventListener("click", () => {
                  title.value = getBookObj(i).title;
                  author.value = getBookObj(i).author;
                  genre.value = getBookObj(i).genre;
                  info.value = getBookObj(i).info;
                  imageChoice = getBookObj(i).image;
                  editArticle.querySelector(
                    "#addImage"
                  ).style.backgroundImage = `url(${imageChoice})`;
                  editArticle
                    .querySelector("#addImage")
                    .classList.add("active");
                  editArticle
                    .querySelector("#bookSuggestions")
                    .querySelectorAll(".bookFetch")
                    .forEach((book) => {
                      book.remove();
                    });
                });
              });
            }
          })
          .catch()
          
      
        } else {
          editArticle.querySelector("#bookSuggestions").style.display = "none"
        editArticle
          .querySelector("#bookSuggestions")
          .querySelectorAll(".bookFetch")
          .forEach((book) => {
            book.remove();
          });
      }
      
    })
  ;

    //Klickar man på "choose image" så får man möjlighet att välja bild från "https://picsum.photos/" där vi slumpar vilken del vi fetchar. Bytas varje gång man reloadar sidan.
    document.querySelector("#addImage").addEventListener("click", () => {
      const imgBox = document.createElement("div");
      imgBox.id = "imgBox";
      randomimages.forEach((img) => {
        imgBox.appendChild(img);
        img.addEventListener("click", () => {
          imageChoice = img.src;
          editArticle.querySelector(
            "#addImage"
          ).style.backgroundImage = `url(${imageChoice})`;
          editArticle.querySelector("#addImage").classList.add("active");
          imgBox.remove();
        });
        editArticle.appendChild(imgBox);
      });
    });
    //submitar formuläret så att vi skapar en ny book i griden.
    const form = document.querySelector("#addForm");
    form.addEventListener("submit", (e) => {
      e.preventDefault();
      const book = {
        // sätter upp begränsningar så användaren ej kan lägga till html taggar
        title: e.target.title.value.replace(/<[^>]*>/g, ""),
        author: e.target.author.value.replace(/<[^>]*>/g, ""),
        genre: e.target.genre.value.replace(/<[^>]*>/g, ""),
        info: e.target.info.value.replace(/<[^>]*>/g, ""),
        img: imageChoice,
      };
      bookSection.removeChild(bookSection.lastChild);
      hideX();
      editBtn.disabled = false;
      addBook("beforeend", book);
    });
    // Man klickar tillbaka från input fälten till edit startsidan.
    document.getElementById("arrow").addEventListener("click", () => {
      bookSection.removeChild(bookSection.lastChild);
      hideX();
      editMode();
    });
  });
}

// https://picsum.photos/
const randomnumber = Math.floor(Math.random() * 50);
const randomimages = [];

fetch(`https://picsum.photos/v2/list?page=${randomnumber}&limit=12`)
  .then((res) => res.json())
  .then((data) => {
    data.forEach((obj) => {
      const newUrl = obj.download_url.split("/");
      newUrl[newUrl.length - 1] = "512";
      newUrl[newUrl.length - 2] = "512";
      const imageElement = document.createElement("img");
      imageElement.src = newUrl.join("/");
      randomimages.push(imageElement);
    });
  });
